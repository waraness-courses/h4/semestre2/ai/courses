# Exercises

## Exercise 1
fitness values: 5 10 15 25 50 100

**Roulette Wheel**
| Individual | Fitness | Probability | Number of copies |
| ----------- | ----------- | ----------- | ----------- |
| A | 5 | 0.024 | 0.144 | 
| B | 10 | 0.049 | 0.30 |
| C | 15 | 0.073 | 0.43 -> 1 copy of Ind3 |
| D | 25 | 0.122 | 0.73 -> 1 copy of Ind4 |
| E | 50 | 0.244 | 1.46 -> 1 copy of Ind5 / 2 |
| F | 100 | 0.488 | 2.92 -> 3 copy of Ind3 |

Probability = $\frac{f_i}{\sum_{i=1}^{6} f_i}  = \frac{f_i}{205}$
*ex: $P_A = \frac{5}{205}$*

Number of copies = $6 \times \frac{f_i}{\sum_{i=1}^{6} f_i} = 6 \times probability$
*ex: $NOC_A = 0.14 \times 6$*

## Exercise 2

| Individual | Genotype | Fitness | Probability | Number of copies |
| ----------- | ----------- | ----------- | ----------- | ----------- |
| A | 0111 | 1 | 0.14 | 1.14 -> 1 ind of A | 
| B | 1011 | 1 | 0.14 | 1.14 -> 1 ind of B | 
| C | 1010 | 2 | 0.29 | 2.28 -> 2 ind of C | 
| D | 1101 | 3 | 0.43 | 3.43 -> 4 ind of D | 

Probability = $\frac{f_i}{\sum_{i=1}^{6} f_i}  = \frac{f_i}{7}$
*ex: $P_A = \frac{1}{7}$*

Number of copies = $8 \times \frac{f_i}{\sum_{i=1}^{6} f_i} = 8 \times probability$
*ex: $NOC_A = 0.14 \times 8$*


## Exercise 3

**1. Encoding schema**
We're going to use a binary representation on 5 bits. Because we have 32 possible values (interval from 0 to 31)

| Individual | Phenotype = Fitness | Genotype |
| ----------- | ----------- | ----------- |
| 1 | 11 | 010111 |
| 2 | 19 | 10011 |
| 3 | 5 | 00101 |
| 4 | 24 | 11000|


**2. A biased RWS is turned 4 times. Find which individuals are selected for crossover and how many times.**
Sum of Fitness = 59

| Individual | Phenotype = Fitness | Genotype | Probability  | Number of copies |
| ----------- | ----------- | ----------- | ----------- |  ----------- |
| 1 | 11 | 010111 | 0.19 | 0.76 -> 1 ind of 1 |
| 2 | 19 | 10011 | 0.32 | 1.28 -> 1 ind of 2 |
| 3 | 5 | 00101 | 0.08 | 0.32 |
| 4 | 24 | 11000| 0.41 | 1.64 -> 2 ind of 4 |


Individuals chosen for crossover are the following. We reasign new ids to our selected genotypes to continue.
| Individual | Genotype |
| ----------- | ----------- |
| 1 | 11000 (was 4) |
| 2 | 11000 (was 4) |
| 3 | 10011 (was 2) |
| 4 | 01011 (was 1) |


**3. For the crossover, the couples are randomly chosen. Apply a one point crossover such as the cross point is the second gene and the parents are the 2nd and 3rd selected parents.**
Crossover between 2 an 3 to the second gene (we ALWAYS start from the right, the less significant bit).

110 **|** 00 
100 **|** 11

11011
10000

**4. Apply a one point crossover such as the cross point is the third gene and the parents are the 1st and 4th selected parents.**

Crossover between 1 an 4 to the third gene (we ALWAYS start from the right, the less significant bit).

11 **|** 000
01 **|** 011

11011
01000

**5. The off-springs of the 1st and 4th selected parents are mutated such as the 3rd gene is set to 1.**
11111 of 3
01100 of 4

**6. Evaluate the new generation (fitness value of off-springs) and compare to initial population.**

| | Offsprings / Indvidual | Phenotype = Fitness |
| - | - | - |
| 1 | 11011 | 28 |
| 2 | 10000 | 16 |
| 3 | 11111 | 31 |
| 4 | 01100 | 12 |

The max fitness has increase (31 vs 24).
The global fitness (sum) has also increase (86 vs 59).


## Exercise 4
| Sequence | Value x | Fitness u(x) | % of chance of reproduction | Cumulated percentage |
| - | - | - | - | - |
| 10111010 | 0,727 | 0,795 | 0,306 | 0,306 |
| 11011110 | 0,867 | 0,461 | 0,177 | 0,484 |
| 00011010 | 0,102 | 0,365 | 0,141 | 0,624 |
| 01101100 | 0,422 | 0,976 | 0,376 | 1,000 |

[SpreadSheet](https://docs.google.com/spreadsheets/d/1AcOU5zlUVVV_qrdTXHvmc2vOFAnL19Kx9WL0enIuiJY/edit?usp=sharing) with formulas and values.


**1. The selection for the crossover is made using a RWS. We suppose that the following 4 numbers are randomly drawn: 0,47 0,18 0,89 and 0,5. Which individuals are then selected?**
0,47 -> Ind 2
0,18 -> Ind 1
0,89 -> Ind 4
0.50 -> Ind 3


**2. Find the off-springs of the 2 crossovers (parent 1 and parent 2 parent 3 and parent 4) by applying a one point crossover such that the 5th gene is the cross point.**
We need to define parents based on the previous question

| Parent | Sequence |
| - | - |
| 1 | 11011110 |
| 2 | 10111010 |
| 3 | 01101100 |
| 4 | 00011010 |

**parent 1 and parent 2:**
110 **|** 11110
101 **|** 11010 

11011010 
10111110


**parent 3 and parent 4:**
011 **|** 01100
000 **|** 11010

01111010
00001100


**3. Evaluate the new generation and compare it with the initial population.**

| Sequence | Value x | Fitness u(x) |
| - | - | - |
| 11011010 | 0,852 | 0,506 |
| 10111110 | 0,742 | 0,765 |
| 01111010 | 0,477 | 0,998 |
| 00001100 | 0,047 | 0,179 |

The average fitness has decreased (0,612 vs 0,649). Here the algorithm is not efficient, we need to change strategy.

[SpreadSheet](https://docs.google.com/spreadsheets/d/1AcOU5zlUVVV_qrdTXHvmc2vOFAnL19Kx9WL0enIuiJY/edit?usp=sharing) with formulas and values.

## Exercise 5
Check out the code source on this [gitlab repo](https://gitlab.com/waraness-courses/h4/semestre2/ai/td/03-05-travelling-salesman-problem)