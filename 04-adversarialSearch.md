# Exercises

## Exercise 1
![exercise 1](./more-resources/images/04-01.png)


## Exercise 2
![exercise 2](./more-resources/images/04-02.png)

## Exercise 3
![exercise 3.1](./more-resources/images/04-03-01.png)

![exercise 3.2](./more-resources/images/04-03-02.png)

## Exercise 4
![exercise 4](./more-resources/images/04-04.png)

## Exercise 5
![exercise 4](./more-resources/images/04-05.png)