# Exercises

## Exercise 1
- River representation : [leftSide, rightSide]
- F: Fox
- C: Chicken
- S: Sack of chicken-food
- R: Robot

**Initial state**
[FCSRI, _ ]

**States**
[F, RCS], ...
F and C cannot be alone on one side of the river. Same goes for C and S.

**Actions**
[F, RCS] --> [FRC, S]

**Transition model**
On right side: [FCSR, _ ]
GO: (RCS)
Result: [F, RCS]

**Goal test**
[ _ , RFCS]



## Exercise 2
**Initial state**
Empty board

**States**
Queens placed on the board (from 1 to 8) such that we do not have attacking queens.

**Actions**
Place a queen

**Transition model**
The board with the new queen placed on it

**Goal test**
The 8 queens are placed

## Exercise 3
**Initial State** 
A

**iteration = 0**
Frontier = {A}
Explored set = {}

**iteration = 1**
Explored = {A}
Frontier = {BCD}

**iteration = 2**
Explored = {AB}
Frontier = {CDF} 
**G** <-- we've discovered our goal, G, so we stop here

**Solution**
G <- B <- A


## Exercise 4
**iteration = 0**
Explored = {}
Frontier = {A}
**iteration = 1**
Explored = {A}
Frontier = {B(5), C(6), D(7)}
**iteration = 2**
Explored = {A, B}
Frontier = {C(6), D(7), F(8), G(20)}
**iteration = 3**
Explored = {A, B, C}
Frontier = {D(7), F(8), G(20)}
**iteration = 4**
Explored = {A, B, C, D}
Frontier = {F(8), E(11), G(20)}
**iteration = 5**
Explored = {A, B, C, D, F}
Frontier = {E(11), G(15)}
**iteration = 6**
Explored = {A, B, C, D, F, E}
Frontier = {G(15)}
**iteration = 7**
Explored = {A, B, C, D, F, E, G} <-- we've discovered our goal, G, so we stop here
Frontier = {}

**Solution**
Shortest path: 15 Km (7 steps)
G <-F <- B <- A


## Exercise 5
### BFS
**iteration = 0**
Explored = {}
Frontier = {Arad}
**iteration = 1**
Explored = {Arad}
Frontier = {Sibiu, Timisoara, Zerind}
**iteration = 2**
Explored = {Arad, Sibiu}
Frontier = { Timisoara, Zerind, Fagaras, Oradea, Rimnicu Vilcea }
**iteration = 3**
Explored = {Arad, Sibiu, Timisoara}
Frontier = { Zerind, Fagaras, Oradea, Rimnicu Vilcea, Lugoj }
**iteration = 4**
Explored = {Arad, Sibiu, Timisoara, Zerind}
Frontier = { Fagaras, Oradea, Rimnicu Vilcea, Lugoj }
**iteration = 5**
Explored = {Arad, Sibiu, Timisoara, Zerind, Fagaras}
Frontier = { Oradea, Rimnicu Vilcea, Lugoj }
**Bucharest** <-- we've discovered our goal, Bucharest, so we stop here
**Solution**
Bucharest <- Fagras <- Arad
450 km (5 steps)

### Uniform Cost Search
**iteration = 0**
Explored = {}
Frontier = {Arad}
**iteration = 1**
Explored = {Arad}
Frontier = {Zerind(75), Timisoura(118), Sibiu(140)}
**iteration = 2**
Explored = {Arad, Zerind}
Frontier = {Timisoura(118), Sibiu(140), Oradea(146)}
**iteration = 3**
Explored = {Arad, Zerind, Timisoura}
Frontier = {Sibiu(140), Oradea(146), Lugoj(229)}
**iteration = 4**
Explored = {Arad, Zerind, Timisoura, Sibiu}
Frontier = {Oradea(146), Rimnicu Vilcea(220), Lugoj(229), Fagaras(239)}
**iteration = 5**
Explored = {Arad, Zerind, Timisoura, Sibiu, Oradea}
Frontier = {Rimnicu Vilcea(220), Lugoj(229), Fagaras(239)}
**iteration = 6**
Explored = {Arad, Zerind, Timisoura, Sibiu, Oradea, Rimnicu Vilcea}
Frontier = {Lugoj(229), Fagaras(239), Pitesti(317), Craiova(336)}
**iteration = 7**
Explored = {Arad, Zerind, Timisoura, Sibiu, Oradea, Rimnicu Vilcea, Lugoj}
Frontier = {Fagaras(239), Meladia(299), Pitesti(317), Craiova(366) }
**iteration = 8**
Explored = {Arad, Zerind, Timisoura, Sibiu, Oradea, Rimnicu Vilcea, Lugoj, Fagaras}
Frontier = {Meladia(299), Pitesti(317), Craiova(366), Bucharest(450) }
**iteration = 9**
Explored = {Arad, Zerind, Timisoura, Sibiu, Oradea, Rimnicu Vilcea, Lugoj, Fagaras, Meladia}
Frontier = {Pitesti(317), Craiova(366), Drobeta(374), Bucharest(450) }
**iteration = 10**
Explored = {Arad, Zerind, Timisoura, Sibiu, Oradea, Rimnicu Vilcea, Lugoj, Fagaras, Meladia, Pitesti}
Frontier = {Craiova(366), Drobeta(374), Bucharest(418) }
**iteration = 11**
Explored = {Arad, Zerind, Timisoura, Sibiu, Oradea, Rimnicu Vilcea, Lugoj, Fagaras, Meladia, Pitesti, Craiova}
Frontier = {Drobeta(374), Bucharest(418) }
**iteration = 12**
Explored = {Arad, Zerind, Timisoura, Sibiu, Oradea, Rimnicu Vilcea, Lugoj, Fagaras, Meladia, Pitesti, Craiova, Drobeta}
Frontier = { Bucharest(418), Craiova(494) }
**iteration = 13**
Explored = {Arad, Zerind, Timisoura, Sibiu, Oradea, Rimnicu Vilcea, Lugoj, Fagaras, Meladia, Pitesti, Craiova, Drobeta, Bucharest} <-- we've discovered our goal, Bucharest, so we stop here
Frontier = {Craiova(494)} 

**Solution**
Shortest path: 418 Km (13 steps)
Bucharest <- Pitesti <- Rimnicu Vilcea <- Sibiu <- Arad


## Exercise 5
**iteration = 0**
Explored = {}
Frontier = {Arad(336)}
**iteration = 1**
Explored = {Arad}
Frontier = { Sibiu(393), Timisoura(447), Zerind(449) }
**iteration = 2**
Explored = {Arad, Sibiu }
Frontier = { Rimicu Vilea(413), Fagaras(415), Timisoura(447), Zerind(449), Oradea(671) }
**iteration = 3**
Explored = {Arad, Sibiu, Rimicu Vilea }
Frontier = { Fagaras(415), Pitesti(417), Timisoura(447), Zerind(449), Craiova(526), Oradea(671) }
**iteration = 4**
Explored = {Arad, Sibiu, Rimicu Vilea, Fagaras }
Frontier = { Pitesti(417), Timisoura(447), Bucharest(450), Zerind(449), Craiova(526), Oradea(671) }
**iteration = 5**
Explored = {Arad, Sibiu, Rimicu Vilea, Fagaras, Pitesti }
Frontier = { Bucharest(418), Timisoura(447), Zerind(449), Craiova(526), Oradea(671) }
**iteration = 6**
Explored = {Arad, Sibiu, Rimicu Vilea, Fagaras, Pitesti, Bucharest } <-- we've discovered our goal, Bucharest, so we stop here

**Solution**
Shortest path: 418 Km (6 steps)
Bucharest <- Pitesti <- Rimnicu Vilcea <- Sibiu <- Arad